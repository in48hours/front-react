# In48hours frontend

Ce repo contient le code source du frontend de In48hours application test pour indatacore.

## Exigence

- [node.js 16.0.0 ou superieur](https://nodejs.org/en/)

## Structure

- **Dossier** `src/`: contient les differents dossier pour la logique (state / traitement) et le designe (components / page)

---

- **Dossier** `src/redux/`: responsable du management des states de l'application

---

- **Dossier** `src/components/`: contient les differents component de l'application

---

- **Fichier** `index.ts`: c'est le point d'entrer de l'application (main).

---

- **Fichier** `app.ts`: il est responsable de la navigation de l'application

---

##Deploiment
l'application est deployer a l'aide de azure cloud container instance service

- lien api :http://in48hoursback.westeurope.azurecontainer.io:8080/api/ 
- lien swagger : http://in48hoursback.westeurope.azurecontainer.io:8080/swagger-ui/index.html#/
- lien application client : http://in48hours.westeurope.azurecontainer.io:3000/


#### variable d'environnement

- l'url vers l'api <br/>
  `export const BASE_URL = "http://localhost/api"`
- secret pour hasher les données dans local storage<br/>
  `export const SECRET = "mySecret"`

apres avoir configurer le `env.js` il reste qu'a entrer les commandes suivantes

```shell script
$> yarn install
```

```shell script
$> yarn start
```


