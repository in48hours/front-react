import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { store } from "./redux/store";
import { Provider } from "react-redux";
import App from './App'
import { getDataFromLocalStorage, getMe } from './redux/auth/auth_service';
import { NotificationsProvider } from '@mantine/notifications';
import { MantineProvider } from '@mantine/core';



store.dispatch(getDataFromLocalStorage)


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <MantineProvider>
        <NotificationsProvider>
          <App />
        </NotificationsProvider>
      </MantineProvider>
    </Provider>
  </React.StrictMode>, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
