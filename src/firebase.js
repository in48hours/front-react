// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
     apiKey: "AIzaSyB3x_2OQQuIM-rNmVN1W9ZYQqPTf2cWjuM",
     authDomain: "in48hours-c4863.firebaseapp.com",
     projectId: "in48hours-c4863",
     storageBucket: "in48hours-c4863.appspot.com",
     messagingSenderId: "255074383396",
     appId: "1:255074383396:web:e13a0567eba9a28c148ab1",
     measurementId: "G-4LQWJYY738"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
