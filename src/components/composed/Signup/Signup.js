import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { pages } from '../../../enums';
import { useNavigate } from 'react-router-dom';
import { th } from '../../../assets/theme';

function Copyright(props) {
     return (
          <Typography variant="body2" color="text.secondary" align="center" {...props}>
               {'Copyright © '}
               <Link color="inherit" href="https://indatacore.com/">
                    InDataCore
               </Link>{' '}
               {new Date().getFullYear()}
               {'.'}
          </Typography>
     );
}

const theme = createTheme();

export default function SignUp(props) {
     const navigate = useNavigate()

     const handleSubmit = (event) => {
          event.preventDefault();
          const data = new FormData(event.currentTarget);
          props.submitSignUp(data.get('username'), data.get('password'))
     };

     return (
          <ThemeProvider theme={theme}>
               <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <Box
                         sx={{
                              marginTop: 8,
                              display: 'flex',
                              flexDirection: 'column',
                              alignItems: 'center',
                         }}
                    >
                         <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                              <LockOutlinedIcon />
                         </Avatar>
                         <Typography component="h1" variant="h5">
                              Sign up
                         </Typography>
                         <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
                              <Grid container spacing={2}>
                                   <Grid item xs={12}>
                                        <TextField
                                             error={props.auth.warning}
                                             required
                                             fullWidth
                                             id="username"
                                             label="Username"
                                             name="username"
                                             autoComplete="username"
                                        />
                                   </Grid>
                                   <Grid item xs={12}>
                                        <TextField
                                             error={props.auth.warning}

                                             required
                                             fullWidth
                                             name="password"
                                             label="Password"
                                             type="password"
                                             id="password"
                                             autoComplete="new-password"
                                        />
                                   </Grid>

                              </Grid>
                              <Button
                                   type="submit"
                                   fullWidth
                                   variant="contained"
                                   sx={{ mt: 3, mb: 2 }}
                              >
                                   Sign Up
                              </Button>
                              <Grid container justifyContent="flex-end">
                                   <Grid item>
                                        <button style={{
                                             fontFamily: th.fontFamily,
                                             border: 0, background: "transparent",
                                             cursor: "pointer"
                                        }} variant="contained" onClick={() => navigate(`/${pages.SignIn}`)} variant="body2">
                                             {"Already have an account? Sign In"}
                                        </button>
                                   </Grid>
                              </Grid>
                         </Box>
                    </Box>
                    <Copyright sx={{ mt: 5 }} />
               </Container>
          </ThemeProvider>
     );
}