import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { pages } from '../../../enums';
import { useNavigate } from 'react-router-dom';
import { th } from '../../../assets/theme';
import { isNullEmptyOrWhiteSpace } from '../../../Helper';

function Copyright(props) {
     return (
          <Typography variant="body2" color="text.secondary" align="center" {...props}>
               {'Copyright © '}
               <Link color="inherit" href="https://indatacore.com/">
                    InDataCore
               </Link>{' '}
               {new Date().getFullYear()}
               {'.'}
          </Typography>
     );
}

const theme = createTheme();

export default function SignIn(props) {

     const navigate = useNavigate()

     const handleSubmit = (event) => {
          event.preventDefault();
          const data = new FormData(event.currentTarget);
          if (!isNullEmptyOrWhiteSpace(data.get('username'))
               && !isNullEmptyOrWhiteSpace(data.get('password')))
               props.submitLogin(data.get('username'), data.get('password'))
     };

     return (
          <ThemeProvider theme={theme}>
               <Grid container component="main" sx={{ height: '100vh' }}>
                    <CssBaseline />
                    <Grid
                         item
                         xs={false}
                         sm={4}
                         md={7}
                         sx={{
                              backgroundImage: 'url(https://upload.wikimedia.org/wikipedia/commons/6/6a/Eubalaena_glacialis_with_calf.jpg)',
                              backgroundRepeat: 'no-repeat',
                              backgroundColor: (t) =>
                                   t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                              backgroundSize: 'cover',
                              backgroundPosition: 'center',
                         }}
                    />
                    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                         <Box
                              sx={{
                                   my: 8,
                                   mx: 4,
                                   display: 'flex',
                                   flexDirection: 'column',
                                   alignItems: 'center',
                              }}
                         >
                              <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                                   <LockOutlinedIcon />
                              </Avatar>
                              <Typography component="h1" variant="h5">
                                   Sign in
                              </Typography>
                              <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
                                   <TextField
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="username"
                                        label="Username"
                                        name="username"
                                        autoComplete="username"
                                        autoFocus
                                        error={props.auth.warning}

                                   />
                                   <TextField
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        error={props.auth.warning}

                                        autoComplete="current-password"
                                   />
                                   <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        sx={{ mt: 3, mb: 2 }}
                                   >
                                        Sign In
                                   </Button>
                                   <Grid container>

                                        <Grid item>
                                             <button style={{
                                                  fontFamily: th.fontFamily,
                                                  border: 0, background: "transparent",
                                                  cursor: "pointer"
                                             }} variant="contained"
                                                  onClick={() => navigate(`/${pages.SignUp}`)} variant="body2">
                                                  {"Don't have an account? Sign Up"}
                                             </button>
                                        </Grid>
                                   </Grid>
                                   <Copyright sx={{ mt: 5 }} />
                              </Box>
                         </Box>
                    </Grid>
               </Grid>
          </ThemeProvider>
     );
}