import styled from 'styled-components'
import { th } from '../assets/theme'


export const Page = styled.div`
  height: inherit;
  height: 100%;
  background-color:white;
`

export const Row = styled.div`
  display: flex;
  align-items: ${props => props.verti ?? "center"};
  justify-content: ${props => props.hori ?? "center"};
  width: ${props => props.width};
  height: ${props => props.height};
`
export const Column = styled.div`
  display: flex;
  align-items: ${props => props.hori ?? "center"};
  justify-content: ${props => props.verti ?? "center"};
  flex-direction: column;
  width: ${props => props.width};
  height: ${props => props.height};
`

export const MiniText = styled.div`
  font-family: ${th.fontFamily};
  font-size: 11px;
  padding: 0 5px 0 5px;
`

export const Title = styled.h1`
  color: black;
  font-size: large;

  @media screen and (max-width: 400px) {
    font-size: x-large !important;
}
`