import React from 'react';
import { Navigate } from "react-router-dom";

export default function GuardedRoute({ logged, toPage, children }) {
    if (!logged) {
        return <Navigate to={`/${toPage}`} replace />;
    }
    return children;
}

