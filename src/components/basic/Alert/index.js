import { Close } from "../../../assets/icons"
import { Column, MiniText, Row } from "../../style"
import { AlertStyle } from "./style"
import { useSelector } from "react-redux"

export default function Alert(props) {

     const thememode = useSelector(state => state.thememode)
     return <AlertStyle
          style={{ padding: `${props.padding ?? "7px"}` }}
          size={props.size} severity={props.severity}>
          <Column hori="flex-end" verti="center" style={{ width: "100%", height: "100%" }}>
               <Row hori="space-between" height={"25px"} style={{ width: "100%" }}>
                    <MiniText
                         style={{ paddingLeft: "10px", color: `${props.messageColor}` }}>
                         {props.statusCode}
                    </MiniText>
                    <button
                         style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "flex-start",
                              padding: "0", margin: "0"
                         }}
                         fillColor={"red"}
                         hoverColor={"red"}
                         onClick={() => props.onClose(props.severity, props.message, 0)}>
                         <Close style={{ height: "10px", margin: 0, padding: 0 }} />
                    </button></Row>
               <Row hori="center" style={{ width: "100%" }}>{props.severity.icon}{props.message}
               </Row></Column></AlertStyle>

}