import styled from 'styled-components'
import { Link } from "react-router-dom";
import { th } from '../../../assets/theme';


export const LinkText = styled(Link)`
  text-decoration: none;
  font-family: ${th.fontFamily};
  &:active {
    color: aqua;
  }
`


export const Body = styled.div`
  background: #E2DCC8;
  height: 100vh;
  z-index: 1;
  display: flex;
  position: fixed;
  justify-content: space-around;
  align-items: center;
  flex-direction: column;

`