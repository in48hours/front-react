import { useEffect, useState } from "react";
import { pages } from "../../../enums";
import { Body } from "./style";
import { MiniText, } from '../../style'
import { Column } from "../../style";
import { Analytique, File, Home, Logout, Profile, } from '../../../assets/icons'
import { Link, useLocation } from "react-router-dom";
import { logoutAction } from "../../../redux/auth/auth_action";
import { useDispatch } from "react-redux";
import { IconButton } from "@mui/material";
export default function SideBar(props) {

    const [actualPage, setActualPage] = useState(pages.Dashboard)
    const location = useLocation()

    let path = location.pathname.slice(1)

    const dispatch = useDispatch()
    const iconHeight = "25px"
    const iconWidth = "25px"



    useEffect(() => {
        if (path !== actualPage && path !== pages.Login)
            setActualPage(path)
    }, [location])



    return <><Body >

        <Column><Profile style={{ height: iconHeight, width: iconWidth, }} />
            <MiniText style={{ padding: '5px' }}>{props.user?.username}</MiniText>
        </Column>

        <Column>
            <Link style={{ textDecoration: "none", color: "black" }} to={`/${pages.Dashboard}`}>
                <Column hori={"center"} style={{ padding: "5px" }}>
                    <Analytique style={{ height: iconHeight, width: iconWidth, }} />
                    <MiniText
                        style={{ padding: '3px', }}>Dashboard</MiniText>
                </Column>
            </Link>

            <Link style={{ textDecoration: "none", color: "black" }} to={`/${pages.DataTable}`}
                focused={(actualPage === pages.DataTable).toString()}
            >
                <Column hori={"center"} style={{ padding: "5px" }}>
                    <Home style={{ height: iconHeight, width: iconWidth, }} />
                    <MiniText
                        style={{ padding: '3px' }}>DataTable</MiniText>
                </Column></Link>

            <Link style={{ textDecoration: "none", color: "black" }} to={`/${pages.FileUpload}`}>
                <Column hori={"center"} style={{ padding: "5px" }}>
                    <File style={{ height: iconHeight, width: iconWidth, }} />
                    <MiniText
                        style={{ padding: '3px' }}>File upload</MiniText>
                </Column>
            </Link>
        </Column>

        <div />

        <Column style={{ cursor: "pointer" }} onClick={() => dispatch(logoutAction())}>
            <Logout style={{ height: iconHeight, width: iconWidth, }} />
            <MiniText
                style={{ padding: '5px', color: "black" }}>logout</MiniText>
        </Column>
    </Body >
        <div style={{ height: "100vh", minWidth: "81px" }} />
    </>
}