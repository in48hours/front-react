import { CircularProgress, } from "@mui/material";
import { useEffect } from 'react'
import { Error, Info, Success, Warning } from "../../../assets/icons";
import { showNotification, updateNotification } from "@mantine/notifications";
import { th } from "../../../assets/theme";
export default function Scaffold(props) {

    const updateNotif = (title, message, icon,
    ) => updateNotification({
        id: 'notif',
        autoClose: 5000,
        title: title,
        message: message,
        icon: icon,
        loading: false
    });

    const showNotif = (title, message,) => showNotification({
        id: 'notif',
        title: title,
        message: message,

        loading: true,
        autoClose: false,
    });
    const closeNotif = (title, message,) => updateNotification({
        id: 'notif',
        title: title,
        message: message,

        loading: true,
        autoClose: 200,
    });

    const severity = {
        loading: { name: "loading", color: "#d3f2ff", message: `loading...`, icon: <CircularProgress style={{ height: "40px", width: "40px", padding: "10px" }} /> },
        info: {
            color: "#d3f2ff", iconColor: `${th.colorInfo}`,
            icon: <Info style={{
                background: "white",
                fill: `${th.colorInfo}`, height: "30px"
            }} />
        },
        error: {
            color: "#fce0e0", iconColor: `${th.colorError}`,
            icon: <Error style={{
                background: "white",
                fill: `${th.colorError}`, width: "50px", height: "50px"
            }} />
        },
        warn: {
            color: "#ffeed8", iconColor: `${th.colorWarning}`,
            icon: <Warning style={{
                background: "white",
                fill: `${th.colorWarning}`, height: "50px"
            }} />
        },
        success: {
            color: "#d8ffd8", iconColor: `${th.colorSuccess}`,
            icon: <Success style={{
                background: "white",
                fill: `${th.colorSuccess}`, height: "50px"
            }} />
        },
    }





    useEffect(() => {
        if (props.error && props.error.code) {
            const response = props.error
            if (response.status >= 300) {

                updateNotif(response.status, response.message, severity.error.icon,
                    'black', severity.error.color)


            } else if (response.status < 300) {
                updateNotif(response.status, response.message, severity.success.icon,
                    'black', severity.success.color)
            }


        } else if (props.error.loading === true) {
            showNotif("loading...", " ",
                severity.loading.color)
        }
        else
            closeNotif("loading...", " ",
                severity.loading.color)


    }, [props.error])





    function render() {
        //success
        return <div style={{
            backgroundColor: "white",
            display: "flex", flexDirection: "column",
            width: '100vw', height: "100vh"
        }}>
            <div>{props.children}</div></div>
    }

    return render()
}