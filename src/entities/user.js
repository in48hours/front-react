import { AES, enc } from "crypto-js"
import { SECRET } from "../env";

export default class User {

    static accessToken = '';
    static username = '';

    static storeData() {
        //crypt
        const accessToken = AES.encrypt(this.accessToken, SECRET);
        const username = AES.encrypt(this.username, SECRET);

        localStorage.setItem('accessToken', accessToken)
        localStorage.setItem('username', username)

    }

    static async getData() {
        //decrypt
        const username = localStorage.getItem('username');
        const accessToken = localStorage.getItem('accessToken')
        if (accessToken && username) {
            this.accessToken = (await AES.decrypt(accessToken, SECRET)).toString(enc.Utf8);
            this.username = (await AES.decrypt(username, SECRET)).toString(enc.Utf8);
            return this.toJson()
        } else {
            this.accessToken = null
            this.username = null
        }
    }

    static toJson() {
        return {
            username: this.username,
            accessToken: this.accessToken,
        }
    }
}