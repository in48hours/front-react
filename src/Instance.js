import axios from "axios";
import { throwAxiosError } from "./errors/errorManager";
import { BASE_URL } from "./env";
import User from "./entities/user";

export default class Instance {

    static getAxios() {
        if (this.axiosInstance)
            return this.axiosInstance
        else {
            this.axiosInstance = axios.create();
            this.axiosInstance.interceptors.request
                .use(function (config) {
                    config.headers = {
                        Accept: '*/*',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${User.accessToken}`
                    }
                    config.validateStatus = () => {
                        return true;
                    };
                    config.baseURL = `${BASE_URL}`
                    config.timeout = 15000
                    return config
                }, function (error) {
                    throwAxiosError(error)
                });
            return this.axiosInstance
        }
    }

}