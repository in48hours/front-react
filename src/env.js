let baseUrl = "http://in48hoursback.westeurope.azurecontainer.io:8080/api/"
let secret = "22f88314-9919-4140-9f7f-d439f6f9c2b2"

if (process.env.REACT_APP_STAGE === "dev") {
     baseUrl = "http://localhost:8080/api/"
     secret = "22f88314-9919-4140-9f7f-d439f6f9c2b2"
}
export const BASE_URL = baseUrl
export const SECRET = secret