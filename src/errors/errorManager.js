import { errorAction } from "../redux/error/error_action";
export function throwAxiosError(error) {
    if (error.response)
        if (error.response.data?.message)
            throw {
                message: error.response.data.message,
                code: error.response.data.code,
            }
    throw {
        message: error.response.statusText,
        code: error.response.status,
    }
}

//catch the backend error
export function throwBackError(data, dispatch) {
    // TODO traitement code error
    if (data.status > 300) {

        dispatch(errorAction({
            message: data.message,
            code: data.code,
            status: data.status,
            param: data.param
        }))
    }
}