export { ReactComponent as Driver } from './cubes-solid.svg'
export { ReactComponent as Analytique } from './chart-line-solid.svg'
export { ReactComponent as Home } from './house-solid.svg'
export { ReactComponent as Configuration } from './sliders-solid.svg'
export { ReactComponent as Profile } from './address-card-solid.svg'
export { ReactComponent as Scan } from './satellite-dish-solid.svg'
export { ReactComponent as Logout } from './arrow-right-from-bracket-solid.svg'
export { ReactComponent as User } from './user-solid.svg'
export { ReactComponent as Delete } from './trash-can-solid.svg'
export { ReactComponent as Setting } from './gear-solid.svg'
export { ReactComponent as Search } from './magnifying-glass-solid.svg'
export { ReactComponent as History } from './clock-rotate-left-solid.svg'
export { ReactComponent as Satellite } from './satellite-solid.svg'
export { ReactComponent as Close } from './xmark-solid.svg'
export { ReactComponent as ArrowDown } from './angle-down-solid.svg'
export { ReactComponent as ArrowLeft } from './angle-left-solid.svg'
export { ReactComponent as Reset } from './delete-left-solid.svg'
export { ReactComponent as Play } from './play-solid.svg'
export { ReactComponent as Read } from './book-open-solid.svg'
export { ReactComponent as Write } from './pen-clip-solid.svg'
export { ReactComponent as Network } from './network-wired-solid.svg'
export { ReactComponent as Dotes } from './ellipsis-solid.svg'
export { ReactComponent as Retry } from './retweet-solid.svg'
export { ReactComponent as Chrono } from './stopwatch-solid.svg'
export { ReactComponent as Ethernet } from './ethernet-solid.svg'
export { ReactComponent as Signal } from './signal-solid.svg'
export { ReactComponent as Connexion } from './tower-cell-solid.svg'
export { ReactComponent as Max } from './up-down-solid.svg'
export { ReactComponent as Delay } from './hourglass-solid.svg'
export { ReactComponent as Bug } from './bug-solid.svg'
export { ReactComponent as Success } from './circle-check-solid.svg'
export { ReactComponent as Info } from './circle-info-solid.svg'
export { ReactComponent as Warning } from './triangle-exclamation-solid.svg'
export { ReactComponent as Error } from './circle-exclamation-solid.svg'
export { ReactComponent as NotFound } from './404.svg'
export { ReactComponent as Mini } from './minimize-solid.svg'
export { ReactComponent as Maxi } from './maximize-solid.svg'
export { ReactComponent as ChartArea } from './chart-area-solid.svg'
export { ReactComponent as Group } from './object-group-solid.svg'
export { ReactComponent as UnGroup } from './object-ungroup-solid.svg'
export { ReactComponent as Fr } from './fr-flag.svg'
export { ReactComponent as En } from './en-flag.svg'
export { ReactComponent as File } from './file-arrow-up-solid.svg'











