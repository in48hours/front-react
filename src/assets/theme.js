const colors0 = "#D1D1D1"
const colors1 = "#ff6b6b"
const colors2 = "#19B0F4"
const colors3 = "#FFA117"
const colors4 = "#5CB65F"


export const th = {

    fontFamily: "Karla,Serif",
    colorDisabled: colors0,
    colorError: colors1,
    colorInfo: colors2,
    colorWarning: colors3,
    colorSuccess: colors4
}