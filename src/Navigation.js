import { Outlet, useLocation, useNavigate } from "react-router-dom";
import { Scaffold, SideBar } from "./components";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { loadingAction } from "./redux/error/error_action";
import { pages } from './enums'

export default function Navigation(props) {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const locate = useLocation()
    let path = locate.pathname.slice(1)


    useEffect(() => {
        if (!props.auth?.logged &&
            (path !== pages.SignIn && path !== pages.SignUp)
            || props.auth?.message == "Signed up")
            navigate(`/${pages.SignIn}`)
        else if (path === '')
            navigate(`/${pages.Dashboard}`)

        dispatch(loadingAction(false))
    }, [props.auth]);

    return <div style={{ display: "flex" }}>
        {props.auth?.logged === false ? <div /> : <SideBar user={props.auth?.user} />}
        <Scaffold error={props.error}>
            <Outlet /> </Scaffold>
    </div>
}