import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import {
    Error404Page, DataTablePage, DashboardPage,
    SignUpPage, FileUploadPage, SignInPage
} from "./pages";
import Navigation from "./Navigation";
import { pages } from "./enums";
import { GuardedRoute } from "./components";

function App() {

    const error = useSelector(state => state.error);
    const auth = useSelector(state => state.auth);
    return <>
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Navigation error={error} auth={auth} />}>

                    <Route exact path={`/${pages.Dashboard}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.SignIn}><DashboardPage /></GuardedRoute>} />

                    <Route exact path={`/${pages.DataTable}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.SignIn}><DataTablePage /></GuardedRoute>} />

                    <Route exact path={`/${pages.FileUpload}`}
                        element={<GuardedRoute logged={auth.logged}
                            toPage={pages.SignIn}><FileUploadPage /></GuardedRoute>} />

                    <Route exact path={`/${pages.SignIn}`}
                        element={<GuardedRoute logged={!auth.logged}
                            toPage={pages.Dashboard}><SignInPage
                                auth={auth} /></GuardedRoute>} />

                    <Route exact path={`/${pages.SignUp}`}
                        element={<GuardedRoute logged={!auth.logged}
                            toPage={pages.Dashboard}><SignUpPage
                                auth={auth} /></GuardedRoute>} />

                    <Route path='*' element={<Error404Page />} />
                </Route>
            </Routes>
        </BrowserRouter>
    </>
}

export default App
