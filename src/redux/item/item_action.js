export const GET_ITEM = 'GET_ITEM'
export const CREATE_ITEM = "CREATE_ITEM"
export const DELETE_ITEM = "DELETE_ITEM"
export const CREATE_RANDOM_ITEM = "CREATE_RANDOM_ITEM"
export function getAllItemAction(items) {
    return {
        type: GET_ITEM,
        payload: items,
    }
}

export function deleteItemAction(ids) {
    return {
        type: DELETE_ITEM,
        payload: ids,

    }
}

export function createRandomItemAction(item) {
    return {
        type: CREATE_RANDOM_ITEM,
        payload: item,

    }
}
