import { CREATE_RANDOM_ITEM, DELETE_ITEM, GET_ITEM } from "./item_action";

const initialState = {}


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case GET_ITEM:
            return action.payload;

        case CREATE_RANDOM_ITEM:
            return { donne: [...state.donne, action.payload] }
        case DELETE_ITEM:
            return {

                donne: state.donne.filter((e) => {
                    return !action.payload.includes(e.id)
                }
                )

            }

        default:
            return state
    }
}

