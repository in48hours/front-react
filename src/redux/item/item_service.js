import { errorAction, loadingAction, successAction, } from "../error/error_action";
import { throwBackError } from "../../errors/errorManager";
import Instance from "../../Instance";
import { createRandomItemAction, deleteItemAction, getAllItemAction } from "./item_action";



export const getAllItem = () => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.get('items');
            dispatch(loadingAction(false))

            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else
                dispatch(getAllItemAction({ donne: res.data.donne }))
        } catch (e) {
            dispatch(errorAction({
                message: e.message,
                status: 500
            }))
        }
    }
}


export const deleteItems = (ids) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.delete(`items/bulk`,
                { data: { ids: ids } });
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else {
                dispatch(successAction(res.data))
                dispatch(deleteItemAction(res.data.donne))
            }
        } catch (e) {
            dispatch(errorAction())
        }
    }
}


export const createRandomItem = () => {
    return async (dispatch) => {
        try {

            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.post(`items/random`,
                {});
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            //success
            else {
                dispatch(successAction(res.data))
                dispatch(createRandomItemAction(res.data.donne))
            }
        } catch (e) {
            dispatch(errorAction())
        }
    }
}

