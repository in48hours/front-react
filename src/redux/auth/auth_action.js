
export const LOGGED_IN = 'LOGGED_IN'
export const EMAIL_PASSWORD_INC = 'EMAIL_PASSWORD_INC'
export const LOGOUT = 'LOGOUT'
export const GET_DATA_STORAGE = 'GET_DATA_STORAGE'
export const SIGNED_UP = "SIGNED_UP"

export function loggedInAction(user) {
    return {
        type: LOGGED_IN,
        payload: {
            logged: true,
            message: 'Authenticated',
            warning: false,
            user: user,
        },
    }
}

export function signUpAction(user) {
    return {
        type: SIGNED_UP,
        payload: {
            logged: false,
            message: 'Signed up',
            warning: false,
            user: user,
        },
    }
}


export function getDataFromLocalStorageAction(user) {
    return {
        type: GET_DATA_STORAGE,
        payload: {
            logged: user?.accessToken !== undefined,
            warning: false,
            user: user
        }
    }
}

export function logoutAction() {
    return {
        type: LOGOUT,
        payload: {
            logged: false,
            warning: false
        }
    }
}

export function emailOrPasswordInc() {
    return {
        type: EMAIL_PASSWORD_INC,
        payload: {
            logged: false,
            message: 'username or password are incorrect',
            warning: true
        }
    }
}