import { errorAction, loadingAction, } from "../error/error_action";
import { throwBackError } from "../../errors/errorManager";
import Instance from "../../Instance";
import { emailOrPasswordInc, getDataFromLocalStorageAction, loggedInAction, signUpAction } from "./auth_action";
import User from "../../entities/user";


export const login = (username, password) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.post('authenticate',
                {
                    username: username,
                    password: password,
                });
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                //invalid password or username not found
                if (res.data.code === 4 || res.data.code === 3)
                    dispatch(emailOrPasswordInc())
                else
                    throwBackError(res.data, dispatch)
            }
            //success
            else
                dispatch(loggedInAction(res.data.donne))
        } catch (e) {
            dispatch(errorAction());
        }
    }
}


export const SignUpThunk = (username, password) => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.post('users',
                {
                    username: username,
                    password: password,
                });
            dispatch(loadingAction(false))
            if (res.data.status > 300) {
                // code 5 for username already userd
                if (res.data.code === 5)
                    dispatch(emailOrPasswordInc())
                else
                    throwBackError(res.data, dispatch)
            }
            //success
            else
                dispatch(signUpAction(res.data.donne))
        } catch (e) {
            dispatch(errorAction());
        }
    }
}

export const getDataFromLocalStorage = async (dispatch) => {
    try {
        dispatch(loadingAction())
        const user = await User.getData()
        dispatch(getDataFromLocalStorageAction(user))
        dispatch(loadingAction(false))

    } catch (e) {
        dispatch(errorAction({ code: 23 }));
    }
}







