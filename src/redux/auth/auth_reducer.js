import { EMAIL_PASSWORD_INC, GET_DATA_STORAGE, LOGGED_IN, LOGOUT, SIGNED_UP } from './auth_action'
import User from "../../entities/user";


//getting data from localstorage

const initialState = {
    logged: User?.accessToken !== undefined,
    warning: false,
    user: User.toJson()
}


export default function reducer(state = initialState, action) {

    switch (action.type) {
        case LOGGED_IN:
            User.accessToken = action.payload.user.accessToken
            User.username = action.payload.user.username
            User.storeData();
            action.payload.user = User.toJson()
            return action.payload
        case SIGNED_UP:
            return action.payload
        case EMAIL_PASSWORD_INC:
            return action.payload
        case LOGOUT:
            localStorage.clear()
            return action.payload
        case GET_DATA_STORAGE:
            return action.payload
        default:
            return state
    }
}

