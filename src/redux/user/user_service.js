import { getUserAction } from './user_action'
import { errorAction, loadingAction } from "../error/error_action";
import Instance from "../../Instance";
import { throwBackError } from "../../errors/errorManager";


export const getUsers = () => {
    return async (dispatch) => {
        try {
            dispatch(loadingAction())
            const axios = Instance.getAxios()
            const res = await axios.get('users',)
            dispatch(loadingAction(false));

            if (res.data.status > 300) {
                throwBackError(res.data, dispatch)
            }
            else
                //success
                dispatch(getUserAction([...res.data.donne]));

        } catch (e) {
            dispatch(errorAction())
        }
    }
}


