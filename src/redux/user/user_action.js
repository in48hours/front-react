export const GET_USERS = 'GET_USERS'
export const USER_COUNT = "USER_COUNT"

export function getUserAction(users) {
    return {
        type: GET_USERS,
        payload: users,
    }
}

export function userCountAction(count) {
    return {
        type: USER_COUNT,
        payload: count,
    }
}