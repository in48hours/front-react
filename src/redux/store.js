
import { configureStore } from '@reduxjs/toolkit'
import { user } from "./user"
import { error } from './error'
import { auth } from './auth'
import { item } from './item'

export const store = configureStore({

     reducer: {
          user,
          error,
          auth,
          item,
     }
})


