import { NotFound } from "../assets/icons";
import { Center, } from "../components";
import { Column, Page } from "../components/style";
import styled from 'styled-components'

const Titler = styled.h1`
color:"black";

@media screen and (max-width: 900px) {
     font-size: x-large !important;
}
@media screen and (max-width: 715px) {
     font-size: large !important;
}
@media screen and (max-width: 585px) {
     font-size: 15px !important;
}
`

const Pic = styled(NotFound)`
fill:black;
@media screen and (max-width: 900px) {
     height: 300px !important;
}
@media screen and (max-width: 715px) {
     height: 200px !important;
}
@media screen and (max-width: 585px) {
     height: 150px !important;
}
`

const SubTitle = styled.p`
color:black;

@media screen and (max-width: 900px) {
     font-size: medium !important;
}
@media screen and (max-width: 715px) {
     font-size: small !important;
}
@media screen and (max-width: 585px) {
     font-size: x-small !important;
}
`

export default function Error404Page() {
     return <Page
          style={{ marginTop: "20px" }}
     >
          <Center>
               <Column>
                    <Pic />
                    <Titler >
                         The page you were looking for doesn't exist.
                    </Titler>
                    <SubTitle >
                         You may have mistyped the address or the page may have moved.</SubTitle>
               </Column></Center></Page>
}