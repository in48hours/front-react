import React from 'react';
import { useDispatch } from 'react-redux';
import SignUp from '../components/composed/Signup/Signup';
import { SignUpThunk } from '../redux/auth/auth_service';
import { loadingAction } from '../redux/error/error_action';

export default function SignUpPage(props) {



     const dispatch = useDispatch()
     const submitSignUp = (email, mdp) => {
          dispatch(loadingAction());
          dispatch(SignUpThunk(email, mdp))
     }
     return <SignUp
          auth={props.auth} submitSignUp={submitSignUp}></SignUp>
}