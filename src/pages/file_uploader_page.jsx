
import { useEffect, useState } from "react";
import { storage } from '../firebase';
import { ref, getDownloadURL, deleteObject, listAll, uploadBytesResumable } from "firebase/storage";
import { Column } from "../components/style";
import { Delete } from "@mui/icons-material";
import { v4 } from "uuid";
import { Button, Grid } from "@mui/material";

export default function FileUploadPage(props) {

  const [imgUrl, setImgUrl] = useState(null);
  const [progresspercent, setProgresspercent] = useState(0);
  const [files, setFiles] = useState([]);

  useEffect(() => {
    const fetchImages = async () => {
      let result = ref(storage, "files")

      listAll(result)
        .then(async (res) => {
          let urlPromises = res.items.map((itemRef) => {

            return getDownloadURL(itemRef)
              .then((url) => {
                return { url: url, ref: itemRef }
              })
          })
          setFiles(await Promise.all(urlPromises))




        }).catch((error) => {
          console.log(error)
        });
    }

    fetchImages()

  }, [])

  const deleteHandler = (e) => {

    const desertRef = ref(storage, `${e.ref._location.path_}`);

    // Delete the file
    deleteObject(desertRef).then(() => {
      setFiles(files.filter(f => f.ref._location.path_ != e.ref._location.path_))
    }).catch((error) => {
      // Uh-oh, an error occurred!
    });
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    const file = e.target[0]?.files[0]
    if (!file) return;
    const storageRef = ref(storage, `files/${file.name}`);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on("state_changed",
      (snapshot) => {
        const progress =
          Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        setProgresspercent(progress);
      },
      (error) => {
        alert(error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
          setFiles([...files, { url: downloadURL, ref: uploadTask.snapshot.ref }])
        });
      }
    );
  }

  return <div style={{ padding: "15px 0 0 5px" }} className="App">
    <form onSubmit={handleSubmit} className='form'>
      <input type='file' />
      <Button variant="contained" type='submit'>Upload</Button>
    </form>
    {
      !imgUrl &&
      <div className='outerbar'>
        <div className='innerbar' style={{ width: `${progresspercent}%` }}>{progresspercent}%</div>
      </div>
    }

    <h1 style={{ padding: "10px" }}>Uploaded file</h1>
    <h3
      style={{ display: "flex", padding: "10px", margin: "20px" }}
    >Total:{files?.length}</h3>
    <Grid container
      justifyContent="flex-start"
      alignItems="flex-start"
      columnSpacing={2}
      rowSpacing={2}
      style={{ width: "90%", display: "flex", border: "solid 1px", padding: "10px", margin: "20px" }}>

      {files?.map((e) => {
        console.log(e)

        return <Grid item xs={1.5}><Column key={v4()}><img style={{ height: "70px" }} src={e.url} />
          <Delete
            onClick={(ev) => { deleteHandler(e) }}
            style={{ cursor: "pointer", height: "20px", fill: "red" }}></Delete></Column></Grid>
      })}</Grid>
  </div>
}