import React from 'react';
import { useDispatch } from 'react-redux';
import SignIn from '../components/composed/Signin/Signin';
import { login } from '../redux/auth/auth_service';
import { loadingAction } from '../redux/error/error_action';

export default function SignInPage(props) {
     const dispatch = useDispatch()
     const submitLogin = (email, mdp) => {
          dispatch(loadingAction());
          dispatch(login(email, mdp))
     }
     return <SignIn auth={props.auth} submitLogin={submitLogin}></SignIn>
}