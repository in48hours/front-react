export { default as DataTablePage } from "./dataTable_page"
export { default as DashboardPage } from "./dashboard_page"
export { default as FileUploadPage } from "./file_uploader_page"
export { default as SignUpPage } from './signup_page'
export { default as SignInPage } from './signin_page'
export { default as Error404Page } from './error_404_page'