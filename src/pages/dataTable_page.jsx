import React, { useEffect, useState } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { useDispatch, useSelector } from 'react-redux';
import { createRandomItem, deleteItems, getAllItem } from '../redux/item/item_service';
import { Center } from '../components';
import { Button, CircularProgress } from '@mui/material';
import { Column, Row } from '../components/style';

export default function DataTablePage() {

     const [ids, setIds] = useState(false)
     const dispatch = useDispatch()
     //get data
     useEffect(() => {
          dispatch(getAllItem())
     }, [])

     const items = useSelector(state => state.item.donne)


     const columns = [
          { field: 'id', headerName: 'ID', width: 70 },
          { field: 'name', headerName: 'Name', width: 130 },
          { field: 'quantity', headerName: 'Quantity', width: 130 },
          { field: 'quantity2', headerName: 'Quantity2', width: 130 },
          { field: 'quantity3', headerName: 'Quantity3', width: 130 },
     ];
     return <Center>
          <Column style={{ padding: "10px" }}>
               <Row hori="space-around" width={"100%"}><Button
                    onClick={() => dispatch(deleteItems(ids))}
                    variant="contained"
                    color="error"
                    disabled={ids == false}
               >Delete</Button>

                    <Button
                         onClick={() => dispatch(createRandomItem())}
                         variant="contained"
                    >Create Random</Button></Row>
               <div style={{ marginTop: "20px", height: "500px", width: '650px' }}>
                    <DataGrid
                         rows={items ?? []}
                         columns={columns}
                         onSelectionModelChange={(e) => { setIds(e) }}
                         pageSize={8}
                         rowsPerPageOptions={[5]}
                         checkboxSelection />

               </div></Column></Center>
}