import { Grid, Skeleton } from '@mui/material';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Bar, BarChart, Pie, PieChart, PolarGrid, PolarAngleAxis, Radar, PolarRadiusAxis, RadarChart, Tooltip, CartesianGrid, Legend, ResponsiveContainer, XAxis, YAxis } from 'recharts';
import { Center } from '../components';
import { Column, Page } from '../components/style';
import { getAllItem } from '../redux/item/item_service';

export default function DashboardPage(props) {


     const dispatch = useDispatch()
     //get data
     useEffect(() => {
          dispatch(getAllItem())
     }, [])

     const items = useSelector(state => state.item.donne)




     const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

     const RADIAN = Math.PI / 180;

     const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
          const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
          const x = cx + radius * Math.cos(-midAngle * RADIAN);
          const y = cy + radius * Math.sin(-midAngle * RADIAN);

          return (
               <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                    {`${(percent * 100).toFixed(0)}%`}
               </text>
          );
     };

     return <Page ><Grid container
          justifyContent="flex-start"
          alignItems="flex-start"
          columnSpacing={5}
          rowSpacing={10}
          style={{ height: "100%", width: "100%", padding: "0px 19px 25px 0px", margin: 0 }}
          columns={{ xs: 3, md: 12, lg: 12, }}>
          <Grid item xs={12} style={{ margin: "0 0 20px 0px", padding: 0, height: '300px', width: "100%" }} >
               <Grid item xs={12}><Center><h2>stacked bar chart</h2></Center></Grid>

               {items?.length > 0 ?
                    <ResponsiveContainer width="100%" height="100%">
                         <BarChart
                              width={500}
                              height={300}
                              data={items}
                              margin={{
                                   top: 20,
                                   right: 30,
                                   left: 20,
                                   bottom: 5,
                              }}
                         >
                              <CartesianGrid strokeDasharray="3 3" />
                              <XAxis dataKey="name" />
                              <YAxis />
                              <Tooltip />
                              <Legend />
                              <Bar dataKey="quantity" stackId="a" fill={COLORS[0]} />
                              <Bar dataKey="quantity2" stackId="a" fill={COLORS[1]} />
                              <Bar dataKey="quantity3" stackId="a" fill={COLORS[2]} />

                         </BarChart>
                    </ResponsiveContainer> : <Center><Column style={{ margin: "0 0 20px 30px", width: "100%" }}>
                         <Skeleton variant="rectangular" style={{
                              width: "100%"
                         }} height={118} />
                         <Skeleton variant="text" width={"100%"} /></Column></Center>}
          </Grid>


          <Grid item xs={5}

               style={{
                    padding: "0", margin: "0 0 10px 30px",
                    width: "100%", height: "350px"
               }}>
               <Center height={"10px"} style={{ marginTop: "100px" }}><h2>Pie</h2></Center>
               {items?.length > 0 ?
                    <ResponsiveContainer width="100%" height="100%">
                         <PieChart >
                              <Pie
                                   data={items}
                                   dataKey={"quantity"}
                                   cx="50%" cy="50%"
                                   outerRadius={35}
                                   fill={COLORS[0]} ></Pie>

                              <Pie
                                   data={items}
                                   dataKey={"quantity2"}
                                   cx="50%" cy="50%"
                                   innerRadius={45}
                                   outerRadius={65}
                                   fill={COLORS[1]} />
                              <Pie
                                   label
                                   data={items}
                                   dataKey={"quantity3"}
                                   cx="50%" cy="50%"
                                   innerRadius={75}
                                   outerRadius={90}
                                   fill={COLORS[2]} />
                              <Tooltip />

                         </PieChart>
                    </ResponsiveContainer> : <Center><Column style={{ width: "90%" }}>
                         <Skeleton variant="circular" style={{
                              width: "50%"
                         }} height={118} />
                         <Skeleton variant="text" width={"100%"} /></Column>
                    </Center>}
          </Grid>

          <Grid item xs={5}
               style={{
                    padding: "0", margin: "0 0 10px 30px",
                    width: "100%", height: "350px"
               }}>
               <Center height={"10px"} style={{ marginTop: "100px" }} ><h2>Radar</h2></Center>

               {items?.length > 0 ?
                    <ResponsiveContainer width="100%" height="100%">
                         <RadarChart cx="50%" cy="50%" outerRadius="80%" data={items}>
                              <PolarGrid />
                              <PolarAngleAxis dataKey="name" />
                              <PolarRadiusAxis />
                              <Radar name="Quantity" dataKey="quantity" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
                              <Radar name="Quantity2" dataKey="quantity2" stroke={COLORS[1]} fill={COLORS[1]} fillOpacity={0.6} />
                              <Radar name="Quantity3" dataKey="quantity3" stroke={COLORS[2]} fill={COLORS[2]} fillOpacity={0.6} />
                              <Tooltip />
                         </RadarChart>
                    </ResponsiveContainer> : <Center><Column style={{ width: "90%" }}>
                         <Skeleton variant="circular" style={{
                              width: "50%"
                         }} height={118} />
                         <Skeleton variant="text" width={"100%"} /></Column>
                    </Center>}</Grid>
     </Grid></Page >
}