export function isIterable(obj) {
    return obj && obj?.length > 0 && !!obj.forEach;
}

export function isNullEmptyOrWhiteSpace(str) {
    return str === null || str?.match(/^\s*$/) !== null || str === "null";
}

export function randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}